# vertexShader
# version 330

layout (location = 0) in vec3 aPos;
layout (location=1) in vec2 uv;

uniform mat4 lightSpaceMatrix;
uniform mat4 modelMatrix;
out vec2 texCoord;

void main()
{
    texCoord = uv;
    gl_Position = lightSpaceMatrix * modelMatrix * vec4(aPos, 1.0);
}

# fragmentShader
# version 330

in vec2 texCoord;
uniform sampler2D textureDiffuseA;

void main()
{
    vec4 diffuse_texture = texture(textureDiffuseA, texCoord);
    
    if (diffuse_texture.a < 0.1)
        discard;
}

