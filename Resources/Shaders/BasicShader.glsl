# fragmentShader
# version 330

struct DirectionalLight
{
//    mat4 spaceMatrix;
    vec3 color;
    float intensity;
    vec3 direction;
    vec3 position;
};

struct PointLight
{
    vec3 color;
    float intensity;
    vec3 position;
    float exponent;
    float linear;
    float constant;
//    mat4 spaceMatrix;
};

struct SpotLight
{
    PointLight base;
    vec3 direction;
    float angle;
    float softness;
};

struct Material
{
    vec3 diffuseColor;
    float diffuse;
    vec3 specularColor;
    float specular;
    vec3 emissionColor;
    float emission;
};

in vec4 vertexColor;
in vec2 texCoord;
in vec3 normal;
in vec3 fragPos;
in vec4 fragLightSpace;

uniform vec3 cameraPosition;

uniform sampler2D textureDiffuseA;
uniform sampler2D textureNormalA;
uniform sampler2D shadowMapA;

uniform Material material;

uniform DirectionalLight directionalLights[4];
uniform int directionalLightsInUse = 0;

uniform PointLight pointLights[4];
uniform int pointLightsInUse = 0;

uniform SpotLight spotLights[4];
uniform int spotLightsInUse = 0;

out vec4 color;

vec3 calculateLightColor(vec3 _color, vec3 _position, vec3 _direction, float _intensity, Material _material)
{
    vec3 lightRay = normalize(_direction - _position);
//    vec3 lightRay = normalize(fragPos - _position);
    
    // shadow
    vec3 projCoord = fragLightSpace.xyz / fragLightSpace.w;
    projCoord = projCoord * 0.5 + 0.5;
//    float closestZ = texture(shadowMapA, projCoord.xy).r;
    float currentZ = projCoord.z;
    float bias = max(0.005 * (1.0 - dot(normal, lightRay)), 0.001);
//    float shadow = currentZ - bias > closestZ  ? 1.0 : 0.0;
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMapA, 0);
    for (int x = -1; x <= 1; ++x)
    {
        for (int y = -1; y <= 1; ++y)
        {
            float closestZ = texture(shadowMapA, projCoord.xy + (vec2(x, y) * texelSize) ).r;
            shadow += currentZ - bias > closestZ  ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;
    
    if (currentZ > 1.0) shadow = 0.0;
    
    // diffuse
    float dotValue = dot(normalize(normal), -lightRay);
    float diffuseFactor = max(dotValue , 0.0f) * _intensity;
    vec3 lightDiffuseValue = _color * diffuseFactor;
    vec3 finalDiffuseColor = lightDiffuseValue * _material.diffuseColor * _material.diffuse;
    
    // specular
    vec3 eyeVec = normalize(cameraPosition - fragPos);
    vec3 lightRayReflect = reflect(lightRay, normalize(normal));
    float specularFactor = dot(eyeVec, lightRayReflect);
    specularFactor = pow(max(specularFactor, 0.0), 20); // shininess
    specularFactor *= diffuseFactor; // spec only where diffuse exists
    vec3 lightSpecularColor = _color * specularFactor;
    vec3 finalSpecularColor = lightSpecularColor * _material.specularColor * _material.specular;
    
    // final color
    return (finalDiffuseColor + finalSpecularColor) * (1.0f - shadow);
}

vec3 CalculateDirectionalLights()
{
    vec3 lightColor = vec3(0.0, 0.0, 0.0);
    for(int i = 0; i < directionalLightsInUse; i++)
    {
        lightColor += calculateLightColor(
                                          directionalLights[i].color,
                                          directionalLights[i].position,
                                          directionalLights[i].direction,
                                          directionalLights[i].intensity,
                                          material
                                          );
    }
    return lightColor;
}


vec3 CalculatePointLight(PointLight light)
{
    vec3 direction = fragPos - light.position;
    float distance = length(direction);
    float decay = 1.0f / (
                          light.constant +
                          (light.linear * distance) +
                          (light.exponent * distance * distance));
    return calculateLightColor(
                               light.color,
                               light.position,
                               direction,
                               light.intensity,
                               material
                               ) * decay;
}


vec3 CalculatePointLights()
{
    vec3 lightColor = vec3(0.0, 0.0, 0.0);
    for(int i = 0; i < pointLightsInUse; i++)
    {
        lightColor += CalculatePointLight(pointLights[i]);
    }
    return lightColor;
}


float remap(float inputValue, float oldMin, float oldMax, float newMin, float newMax)
{
    float move_amount = oldMin - newMin;
    float scale_amount = (newMax - newMin) / (oldMax - oldMin);
    float outputValue = (inputValue - move_amount) * scale_amount;
    return min(max(outputValue, 0.0f), 1.0f);
}

vec3 CalculateSpotLight(SpotLight light)
{
    vec3 lightColor = vec3(0.0, 0.0, 0.0);
    vec3 rayDirection = normalize(fragPos - light.base.position);
    vec3 lightDirection = normalize(light.direction - light.base.position);
    float fac = dot(rayDirection, lightDirection);
    if (fac > light.angle)
    {
        lightColor = CalculatePointLight(light.base);
        lightColor *=  remap(fac, light.angle, 1.0f, 0.0f, 1.0f / max(light.softness, 0.001f));
    }
    return lightColor;
}

vec3 CalculateSpotLights()
{
    vec3 lightColor = vec3(0.0, 0.0, 0.0);
    for(int i = 0; i < spotLightsInUse; i++)
    {
        lightColor += CalculateSpotLight(spotLights[i]);
    }
    return lightColor;
}


float LinearizeDepth(float depth)
{
    float near = 1.0;
    float far = 100.0;
    float z = depth * 2.0 - 1.0;
    return (2.0 * near * far) / (far + near - z * (far - near));
}


void main()
{
    // emission
    vec3 lightColor = material.emissionColor * material.emission;
    
    // directional lights
    lightColor += CalculateDirectionalLights();
    
    // point lights
    lightColor += CalculatePointLights();
    
    // spot lights
    lightColor += CalculateSpotLights();
    
    // use texture
//    color = texture(textureDiffuseA, texCoord) * vec4(lightColor, 1.0);
    vec4 diffuse_texture = texture(textureDiffuseA, texCoord);
    vec4 normal_texture = texture(textureNormalA, texCoord);
    
    if (diffuse_texture.a < 0.1)
        discard;
    
//    color = normal_texture;
    color = diffuse_texture * vec4(lightColor, 1.0);
//    color = texture(shadowMapA, texCoord); //diffuse_texture * vec4(lightColor, 1.0);
    
    
//    vec3 projCoord = (fragLightSpace.xyz / fragLightSpace.w);
//    projCoord = projCoord * 0.5 + 0.5;
//    float closestZ = texture(shadowMapA, projCoord.xy).r;
//    float currentZ = projCoord.z;
//    float shadow = currentZ > closestZ  ? 1.0 : 0.0;
//    color = texture(shadowMapA, texCoord);
    
//    color = vec4(vec3(LinearizeDepth(gl_FragCoord.z) / 100), 1.0);
//    color = vec4(lightColor, 1.0);
}


# vertexShader
# version 330

layout (location=0) in vec3 pos;
layout (location=1) in vec2 uv;
layout (location=2) in vec3 norm;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 lightSpaceMatrix;

out vec4 vertexColor;
out vec2 texCoord;
out vec3 normal;
out vec3 fragPos;
out vec4 fragLightSpace;

void main()
{
    // frag position
    fragPos = (modelMatrix * vec4(pos, 1.0)).xyz;
    
    // position
    gl_Position = projectionMatrix * viewMatrix * vec4(fragPos, 1.0);
    
    // color
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    if (x <= 0.0f && y <= 0.0f && z <= 0.0f) {
        x = 0.2f;
        y = 0.5f;
        z = 0.8f;
    }
    vertexColor = vec4(x, y, z, 1.0);
    
    // uv
    texCoord = uv;
    
    // normal
    normal = mat3(transpose(inverse(modelMatrix))) * norm;
    
    // frag in light space
    fragLightSpace = lightSpaceMatrix * vec4(fragPos, 1.0);
    
}

