# vertexShader
# version 330

layout (location=0) in vec3 pos;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec4 vertex_color;

void main()
{
    // push wireframe a little towards camera so it draws on top of actual geo
    vec4 position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos, 1.0);
    gl_Position = vec4(position.x, position.y, position.z - 0.001f, position.w);
}

# fragmentShader
# version 330
                                             
out vec4 color;
                                          
void main()
{
    color = vec4(0.3, 0.3, 0.3, 0.5);
}

