//
//  GLWindow.cpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-07.
//

#include "GLWindow.hpp"


GLWindow::GLWindow()
{
    for(size_t i = 0; i < 1024; i++)
    {
        keys[i] = false;
    }
}


GLWindow::~GLWindow()
{
}


unsigned int GLWindow::Create(unsigned int windowWidth, unsigned int bufferHeight)
{
    
    // Initialize GLFW
    if(!glfwInit())
    {
        cout << "Failed to initialize GLFW!" << endl;
        glfwTerminate();
        return 1;
    }

    // Define version and compatibility settings
    // OpenGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    // not backward compatible
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // forward compatible
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
//    // not resizable window
//    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create OpenGL window
    id = glfwCreateWindow(windowWidth, bufferHeight, "OpenGL", NULL, NULL);

    // Check for window creation failure
    if (!id)
    {
        cout << "Failed to create GLFW window!" << endl;
        glfwTerminate();
        return 1;
    }
    
    // Get buffer size info
    glfwGetFramebufferSize(id, &width, &height);

    // Set context for GLAD to use
    glfwMakeContextCurrent(id);
    
    // Initialize GLAD
    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        cout << "Failed to initialize GLAD!" << endl;
        glfwDestroyWindow(id);
        glfwTerminate();
        return 1;
    }
      
    // Setup viewport size
    glViewport(0, 0, width, height);
    
    // Inputs
    glfwSetWindowUserPointer(id, this);
    glfwSetKeyCallback(id, HandleKeyboard);
    glfwSetCursorPosCallback(id, HandleMouse);
    
    return 0;
}

bool GLWindow::ShouldClose()
{
    return glfwWindowShouldClose(id);
}


void GLWindow::SwapBuffers()
{
    glfwSwapBuffers(id);
}

bool* GLWindow::getKeys()
{
    return keys;
}

void GLWindow::Close()
{
    glfwSetWindowShouldClose(id, GL_TRUE);
}


GLfloat GLWindow::GetXChange()
{
    GLfloat change = xChange;
    xChange = 0.0f;
    return change;
}


GLfloat GLWindow::GetYChange()
{
    GLfloat change = yChange;
    yChange = 0.0f;
    return change;
}


void GLWindow::HandleKeyboard(GLFWwindow* window, int key, int code, int action, int mode)
{
    
//    // method 1
//    if (key == GLFW_KEY_E)
//    {
//        if (action == GLFW_PRESS)
//        {
//            cout << "E was pressed" << endl;
//        }
//        else if (action == GLFW_RELEASE)
//        {
//            cout << "E was released" << endl;
//        }
//        else if (action == GLFW_REPEAT)
//        {
//            cout << "E was held" << endl;
//        }
//    }
    
    // Method 2
    GLWindow* my_window = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));
    
    if (key == GLFW_KEY_ESCAPE)
    {
        my_window->Close();
    }
        
    if (key >= 0 && key < 1024)
    {
        if (action == GLFW_PRESS)
        {
//            cout << "Pressed: " << key << endl;
            my_window->keys[key] = true;
        }
        if (action == GLFW_RELEASE)
        {
//            cout << "Released: " << key << endl;
            my_window->keys[key] = false;
        }
    }
    
}


void GLWindow::HandleMouse(GLFWwindow* window, double xPos, double yPos)
{
    GLWindow* my_window = static_cast<GLWindow*>(glfwGetWindowUserPointer(window));
    
    if (my_window->mouseFirstMoved)
    {
        my_window->xLast = xPos;
        my_window->yLast = yPos;
    }
    
    my_window->xChange = xPos - my_window->xLast;
    my_window->yChange = my_window->yLast - yPos;
    
    my_window->xLast = xPos;
    my_window->yLast = yPos;
    
    my_window->mouseFirstMoved = false;
    
//    cout << my_window->xChange << ", " << my_window->yChange << endl;
}
