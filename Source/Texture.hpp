//
//  Texture.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-14.
//

#ifndef Texture_hpp
#define Texture_hpp
#include <iostream>
#include <string>
#include <glad.h>
#include <stb_image.h>
#include "Shader.hpp"


using namespace std;


class Texture
{
public:
    Texture(Shader* _shader);
    ~Texture();
    Texture(Shader* _shader, string _filePath, string _type);
    void LoadTexture();
    void UseTexture(Shader* _shader);
//    void DeleteTexture();
//private:
    string filePath;
    int width;
    int height;
    int bitDepth;
    string type;
    GLuint id;
    Shader *shader;
};

#endif /* Texture_hpp */
