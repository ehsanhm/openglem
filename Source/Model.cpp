//
//  Model.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-24.
//

#include "Model.hpp"


void Model::Draw(Shader* shader)
{
    for(size_t i = 0; i < meshes.size(); i++)
    {
        meshes[i].UseTextures(shader);
        meshes[i].Draw();
    }
}


void Model::loadModel(string path)
{
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(
                                             path,
//                                             aiProcess_GenSmoothNormals |
                                             aiProcess_GenNormals |
//                                             aiProcess_FlipUVs |
                                             aiProcess_Triangulate
                                             );
    
    if (!scene || !scene->mRootNode || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE)
    {
        cout << "Failed to load model: " << path << " - " << importer.GetErrorString() << endl;
    }
    
    processNode(scene->mRootNode, scene);
}


void Model::processNode(aiNode *node, const aiScene *scene)
{
    for (size_t i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene));
    }
    
    for (unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene);
    }
}


Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene)
{
//    cout << "=========================\nMesh Name: " << mesh->mName.C_Str() << endl;
    
    // mesh
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    
    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex;
        
        vertex.position.x = mesh->mVertices[i].x;
        vertex.position.y = mesh->mVertices[i].y;
        vertex.position.z = mesh->mVertices[i].z;
        
        if (mesh->HasNormals())
        {
        vertex.normal.x = mesh->mNormals[i].x;
        vertex.normal.y = mesh->mNormals[i].y;
        vertex.normal.z = mesh->mNormals[i].z;
        }
        else
        {
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
        }
        
        if (mesh->mTextureCoords[0])
        {
            vertex.uv.x = mesh->mTextureCoords[0][i].x;
            vertex.uv.y = mesh->mTextureCoords[0][i].y;
        }
        else
        {
            vertex.uv.x = 0.0f;
            vertex.uv.y = 0.0f;
        }
        
        vertices.push_back(vertex);
    }
    
    for (unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; j++)
        {
            indices.push_back(face.mIndices[j]);
        }
    }    
    
    // textures
    vector<Texture> textures = LoadMaterial(mesh, scene);
//    LoadMaterial(mesh, scene);

    
    return Mesh(vertices, indices, textures);
}


vector<Texture> Model::LoadMaterial(aiMesh *mesh, const aiScene *scene)
{
    // textures
    vector<Texture> textures;
//    cout << "Material Index: " << mesh->mMaterialIndex << endl;
    
    if (mesh->mMaterialIndex > 0)
    {
        aiMaterial *aiMat = scene->mMaterials[mesh->mMaterialIndex];
//        cout << "Material Name: " << aiMat->GetName().data << endl;
        
        // diffuse
        for (unsigned int j = 0; j < aiMat->GetTextureCount(aiTextureType_DIFFUSE); j++)
        {
            aiString texturePath;
            aiMat->GetTexture(aiTextureType_DIFFUSE, j, &texturePath);
            
            string textureName = removeSlashes(texturePath.C_Str());
            
            string texturePathStr = "Resources/Textures/" + textureName;
                        
            Texture tex(shader, texturePathStr, "texture_diffuse");
            
            textures.push_back(tex);
        }
        
        // normal
        for (unsigned int j = 0; j < aiMat->GetTextureCount(aiTextureType_HEIGHT); j++)
        {
            aiString texturePath;
            aiMat->GetTexture(aiTextureType_HEIGHT, j, &texturePath);
            
            string textureName = removeSlashes(texturePath.C_Str());
            
            string texturePathStr = "Resources/Textures/" + textureName;
                        
            Texture tex(shader, texturePathStr, "texture_normal");
            
            textures.push_back(tex);
        }
    }
    
    // use white texture if no texture found for the model
    if (textures.size() < 1)
    {
        Texture tex(shader, "Resources/Textures/white.png", "texture_diffuse");        
        textures.push_back(tex);
    }

    return textures;
}


string Model::removeSlashes(const string texturePath)
{
    string textureName = texturePath;
    if (textureName.find("/") != string::npos)
    {
        textureName = textureName.substr(textureName.find_last_of("/") + 1);
    }
    if (textureName.find("\\") != string::npos)
    {
        textureName = textureName.substr(textureName.find_last_of("\\") + 1);
    }
    return textureName;
}


void Model::SetScale(GLfloat scale)
{
    for (size_t i = 0; i < meshes.size(); i++)
    {
//        cout << "default: " << meshes[i].vertices[0].position.x << endl;
        meshes[i] = meshes[i].SetScale(scale);
//        cout << "scaled: " << meshes[i].vertices[0].position.x << endl;
    }
//    Draw();
}
