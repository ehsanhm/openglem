//
//  Mesh.cpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-05.
//

#include "Mesh.hpp"

Mesh::Mesh()
{
    
}

Mesh::Mesh(vector<Vertex> _vertices, vector<unsigned int> _indices, vector<Texture> _textures)
{
    vertices = _vertices;
    indices = _indices;
    textures = _textures;
    
    SetupMesh();
    LoadTextures();
}


void Mesh::SetupMesh()
{
    // VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    
    // EBO
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
    
    // VBO
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
    
    // Vertex attrib
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    
    // UV attrib
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));
    
    // Normal attrib
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    
    // Cleanup
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
//    cout << "Mesh::SetupMesh" << endl;
}


void Mesh:: Draw()
{
    glBindVertexArray(VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(GL_TRIANGLES, (unsigned int)(indices.size()), GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}


void Mesh::LoadTextures()
{
    for (unsigned int i = 0; i < textures.size(); i ++)
    {
//        cout << "Mesh::LoadTextures: " << textures[i].filePath << endl;
        textures[i].LoadTexture();
    }
}

void Mesh::UseTextures(Shader* shader)
{
    for (unsigned int i = 0; i < textures.size(); i ++)
    {
        textures[i].UseTexture(shader);
    }
}

//void Mesh::DeleteMesh()
//{
//    if (IBO != 0)
//    {
//        glDeleteBuffers(1, &IBO);
//    }
//    if (VBO != 0)
//    {
//        glDeleteBuffers(1, &VBO);
//    }
//    if (VAO != 0)
//    {
//        glDeleteVertexArrays(1, &VAO);
//    }
//    indexCount = 0;
//}


Mesh Mesh::SetScale(GLfloat scale)
{
    for (size_t i = 0; i < vertices.size(); i++)
    {
        Vertex vert(
                    vertices[i].position * scale,
                    vertices[i].uv,
                    vertices[i].normal
                    );
        vertices[i] = vert;
    }
    
    SetupMesh();
    return *this;
}


Mesh Mesh::SetTranslate(glm::vec3 translate)
{
    
    for (size_t i = 0; i < vertices.size(); i++)
    {
        Vertex vert(
                    vertices[i].position + translate,
                    vertices[i].uv,
                    vertices[i].normal
                    );
        vertices[i] = vert;
    }
    
    SetupMesh();
    return *this;

}
