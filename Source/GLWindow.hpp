//
//  GLWindow.hpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-07.
//

#ifndef GLWindow_hpp
#define GLWindow_hpp

#include <iostream>
#include <glad.h>
#include <glfw3.h>

using namespace std;


class GLWindow
{
    
public:
    GLWindow();
    ~GLWindow();
    unsigned int Create(unsigned int windowWidth, unsigned int windowHeight);
    bool ShouldClose();
    void Close();
    void SwapBuffers();
    bool* getKeys();
    GLfloat GetXChange();
    GLfloat GetYChange();
    int width;
    int height;
    bool mouseFirstMoved = true;

public:
    static void HandleKeyboard(GLFWwindow* window, int key, int code, int action, int mode);
    static void HandleMouse(GLFWwindow* window, double xPos, double yPos);
    
private:
    GLFWwindow *id;
    bool keys[1024];
    GLfloat xChange = 0.0f;
    GLfloat yChange = 0.0f;
    GLfloat xLast = 0.0f;
    GLfloat yLast = 0.0f;
};

#endif /* GLWindow_hpp */
