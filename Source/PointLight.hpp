//
//  PointLight.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#ifndef PointLight_hpp
#define PointLight_hpp

#include <glad.h>
#include <glm.hpp>
#include "Light.hpp"

class PointLight: public Light
{
public:
    PointLight();
    PointLight(glm::vec3 _color,
               GLfloat _intensity,
               glm::vec3 _position);
    ~PointLight();
    void AddLight(Shader* shader);
    void SetPosition(Shader* shader, glm::vec3 _position);
    GLfloat exponent;
    GLfloat linear;
    GLfloat constant;
};


#endif /* PointLight_hpp */
