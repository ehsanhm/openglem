//
//  Material.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#ifndef Material_hpp
#define Material_hpp

#include <glfw3.h>
#include <glm.hpp>
#include "Shader.hpp"

class Material
{
public:
    Material(Shader& _shader);
    ~Material();
    
    void UseMaterial();
    
    Shader& shader;
    
    glm::vec3 diffuseColor;
    GLfloat diffuse;
    
    glm::vec3 specularColor;
    GLfloat specular;
    
    glm::vec3 emissionColor;
    GLfloat emission;
};

#endif /* Material_hpp */
