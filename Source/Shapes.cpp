//
//  Shapes.cpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-05.
//

#include "Shapes.hpp"


Circle2D::Circle2D(GLfloat radius, GLuint num_points)
{
    Vertex tempVert;
    
    // points on circumference of circle
    for (int i = 0; i < num_points; ++i) {
        GLfloat angle = (360.0f / 57.2958 / num_points) * i;
        
        tempVert.position.x = radius * cos(angle);
        tempVert.position.y = radius * sin(angle);
        tempVert.position.z = 0.0f;
        tempVert.uv.x = (cos(angle) + 1.0f) / 2.0f;
        tempVert.uv.y = (sin(angle) + 1.0f) / 2.0f;
        tempVert.normal.x = 0.0f;
        tempVert.normal.y = 0.0f;
        tempVert.normal.z = 1.0f;
        vertices.push_back(tempVert);
        
        if (i > 0)
        {
            indices.push_back(num_points);
            indices.push_back(i - 1);
            indices.push_back(i);
        }
    }
    
    // center of circle
    tempVert.position.x = 0.0f;
    tempVert.position.y = 0.0f;
    tempVert.position.z = 0.0f;
    tempVert.uv.x = 0.5f;
    tempVert.uv.y = 0.5f;
    tempVert.normal.x = 0.0f;
    tempVert.normal.y = 0.0f;
    tempVert.normal.z = 1.0f;
    vertices.push_back(tempVert);
    
    // last triangle of the circle
    indices.push_back(0);
    indices.push_back(num_points);
    indices.push_back(num_points - 1);
    
    SetupMesh();
}


//Pyramid::Pyramid()
//{
//
//    GLuint ids[] = {
//        0, 1, 2,
//        0, 1, 3,
//        0, 2, 3,
//        1, 2, 3
//    };
//
//    GLfloat verts[] = {
//        // x     y      z           u     v          nx     ny     nz
//        -1.0f, -1.0f,  0.00f,     0.0f,  0.0f,     -1.0f, -1.0f,  0.00f,
//         1.0f, -1.0f,  0.00f,     1.0f,  0.0f,      1.0f, -1.0f,  0.00f,
//         0.0f,  0.8f,  0.90f,     0.5f,  1.0f,      0.0f,  0.8f,  0.90f,
//         0.0f, -1.0f,  1.80f,     0.5f,  0.2f,      0.0f, -1.0f,  1.80f
//    };
//
//    indices = ids;
//    vertices = verts;
//    numVertices = sizeof(verts) / sizeof(verts[0]);
//    numIndices = sizeof(ids) / sizeof(ids[0]);
//    CreateMesh();
//}


Plane::Plane(GLfloat scale)
{

    indices = {
        0, 1, 2,
        0, 2, 3,
    };
    
    Vertex vert;
    
    vert.position = glm::vec3(-1.0f,  0.0f,   1.0f);
    vert.uv = glm::vec2(0.0f,  0.0f);
    vert.normal = glm::vec3(0.0f,  1.0f,  0.0f);
    vertices.push_back(vert);
    
    vert.position = glm::vec3(1.0f,  0.0f,   1.0f);
    vert.uv = glm::vec2(1.0f,  0.0f);
    vert.normal = glm::vec3(0.0f,  1.0f,  0.0f);
    vertices.push_back(vert);
    
    vert.position = glm::vec3(1.0f,  0.0f,  -1.0f);
    vert.uv = glm::vec2(1.0f,  1.0f);
    vert.normal = glm::vec3(0.0f,  1.0f,  0.0f);
    vertices.push_back(vert);
    
    vert.position = glm::vec3(-1.0f,  0.0f,  -1.0f);
    vert.uv = glm::vec2(0.0f,  1.0f);
    vert.normal = glm::vec3(0.0f,  1.0f,  0.0f);
    vertices.push_back(vert);

    SetScale(scale);
//    SetTranslate(glm::vec3(0.0f, -2.0f, 0.0f));
    SetupMesh();
}

