//
//  Transform.cpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-08.
//

#include "Transform.hpp"


Transform::Transform()
{
    matrix = glm::mat4(1.0f);
}


Transform::~Transform()
{
}


void Transform::Translate(float x, float y, float z)
{
    matrix = glm::translate(matrix, glm::vec3(x, y, z));
}


void Transform::RotateAngleAxis(float angle, float x, float y, float z)
{
    matrix = glm::rotate(matrix, angle, glm::vec3(x, y, z));
}


void Transform::Scale(float x, float y, float z)
{
    matrix = glm::scale(matrix, glm::vec3(x, y, z));
}

