//
//  Shadow.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-03.
//

#include "Shadow.hpp"


Shadow::Shadow(GLuint _origWidth, GLuint _origHeight, Light *light, Shader *shader)
{
    width = 1024;
    height = 1024;
    origWidth = _origWidth;
    origHeight = _origHeight;

    glGenFramebuffers(1, &FBO);
    
    glGenTextures(1, &shadowMap);
    glBindTexture(GL_TEXTURE_2D, shadowMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
//    //
//    float near = 0.1f;
//    float far = 50.0f;
    
//    glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near, far);
//    glm::mat4 lightView = glm::lookAt(light->position, light->direction, glm::vec3(0, 1, 0));
//    lightSpaceMatrix = lightProjection * lightView;

}


void Shadow::PrepareToRender()
{
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    
//    shader->UseShader();
//
//    light->SetSpaceMatrix(shader);
}


void Shadow::FinishRender()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, origWidth, origHeight);
}
