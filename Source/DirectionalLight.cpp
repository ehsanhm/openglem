//
//  DirectionalLight.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-17.
//

#include "DirectionalLight.hpp"


void DirectionalLight::AddLight(Shader* shader)
{
    shader->UseShader();
    uid = shader->directionalLightsInUse;
    
    directionId = shader->directionalLightsDirectionsIds[uid];
    positionId = shader->directionalLightsPositionsIds[uid];
    
    glUniform3f(positionId, position.x, position.y, position.z);
    glUniform3f(directionId, direction.x, direction.y, direction.z);    
    glUniform3f(shader->directionalLightsColorsIds[uid], color.x, color.y, color.z);
    glUniform1f(shader->directionalLightsIntensitiesIds[uid], intensity);
    
    shader->directionalLightsInUse ++;
    glUniform1i(shader->directionalLightsInUseId, shader->directionalLightsInUse);
}
