//
//  Texture.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-14.
//

#include "Texture.hpp"


Texture::Texture(Shader* _shader)
{
    width = 0;
    height = 0;
    bitDepth = 0;
    id = 0;
    filePath = "";
    type = "";
    shader = _shader;
}


Texture::~Texture()
{
//    DeleteTexture();
}


Texture::Texture(Shader* _shader, string _filePath, string _type)
{
    filePath = _filePath;
    type = _type;
    shader = _shader;
}


void Texture::LoadTexture()
{
    // load image
    stbi_set_flip_vertically_on_load(true);
    unsigned char* imageData = stbi_load(filePath.c_str(), &width, &height, &bitDepth, 0);
    if (!imageData)
    {
        cout << "Failed to load: " << filePath.c_str() << endl;
        return;
    }
    
    // create gl texture
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    
    // texture wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    // texture filter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    // attach loaded image into texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
    glGenerateMipmap(GL_TEXTURE_2D);
    
//    //
//    if (type == "texture_diffuse")
//    {
//        glActiveTexture(GL_TEXTURE0);
//        shader->SetTextureDiffuseAId(0);
//    }
//    
//    else if (type == "normal_diffuse")
//    {
//        glActiveTexture(GL_TEXTURE1);
//        shader->SetTextureNormalAId(1);
//    }
//    glBindTexture(GL_TEXTURE_2D, id);
    
    // finish
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(imageData);
//    cout << "Texture::LoadTexture: " << id << ", " << filePath << endl;
}


void Texture::UseTexture(Shader* _shader)
{
    shader = _shader;
//    cout << "Texture::UseTexture: " << id  << endl;
    
    if (type == "texture_diffuse")
    {
        glActiveTexture(GL_TEXTURE0);
        shader->SetTextureDiffuseAId(0);
    }
    
    else if (type == "texture_normal")
    {
        glActiveTexture(GL_TEXTURE1);
        shader->SetTextureNormalAId(1);
    }
    
    else if (type == "shadow_map")
    {
        glActiveTexture(GL_TEXTURE2);
        shader->SetInt("shadowMapA", 2);
    }
    
    glBindTexture(GL_TEXTURE_2D, id);
}


//void Texture::DeleteTexture()
//{
//    glDeleteTextures(1, &id);
//    height = 0;
//    width = 0;
//    bitDepth = 0;
//    id = 0;
//    filePath = "";
//    type = "";
//}
