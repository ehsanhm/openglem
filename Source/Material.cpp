//
//  Material.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#include "Material.hpp"

Material::Material(Shader& _shader) : shader(_shader)
{
    diffuseColor = glm::vec3(1.0f, 1.0f, 1.0f);
    diffuse = 1.0f;
    
    specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
    specular = 1.0f;
    
    emissionColor = glm::vec3(0.0f, 0.0f, 0.0f);
    emission = 0.0f;
    
}

Material::~Material()
{
}

void Material::UseMaterial()
{
    shader.SetMaterialDiffuseColor(diffuseColor);
    shader.SetMaterialDiffuse(diffuse);
    shader.SetMaterialSpecularColor(specularColor);
    shader.SetMaterialSpecular(specular);
    shader.SetMaterialEmissionColor(emissionColor);
    shader.SetMaterialEmission(emission);
}
