//
//  Transform.hpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-08.
//

#ifndef Transform_hpp
#define Transform_hpp

#include <iostream>
#include <glm.hpp>
#include <GLM/gtc/type_ptr.hpp>

class Transform
{
public:
    Transform();
    ~Transform();
//    void SetTRS(glm::vec3 t, glm::vec3 r, glm::vec3 s);
    
    void Translate(float x, float y, float z);
    void RotateAngleAxis(float angle, float x, float y, float z);
    void Scale(float x, float y, float z);
    
    glm::vec3 translate;
    glm::vec3 rotate;
    glm::vec3 scale;
    glm::mat4 matrix;
};

#endif /* Transform_hpp */
