//
//  Shadow.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-03.
//

#ifndef Shadow_hpp
#define Shadow_hpp

#include "glad.h"
#include "glm.hpp"
#include "Shader.hpp"
#include "Light.hpp"


class Shadow
{
public:
    Shadow(GLuint _origWidth, GLuint _origHeight, Light *light, Shader *shader);
    ~Shadow(){};

    void PrepareToRender();
    void FinishRender();
    
    GLuint width;
    GLuint height;
    
    GLuint origWidth;
    GLuint origHeight;
    
    GLuint FBO;
    GLuint shadowMap;
    
    glm::mat4 lightSpaceMatrix;
    GLuint lightSpaceMatrixLocation;
    
    Shader *shader;
    Light *light;
};

#endif /* Shadow_hpp */
