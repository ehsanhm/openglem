//
//  Light.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-16.
//

#include "Light.hpp"


Light::Light()
{
    color = glm::vec3(1.0f, 1.0f, 1.0f);
    intensity = 1.0f;
    position = glm::vec3(-1.0f, 1.0f, 1.0f);
    direction = glm::vec3(0.0f, 0.0f, 0.0f);
    angle = 0.0f;
}


Light::Light(glm::vec3 _color, GLfloat _intensity)
{
    color = _color;
    intensity = _intensity;
    position = glm::vec3(-1.0f, 1.0f, 1.0f);
    direction = glm::vec3(0.0f, 0.0f, 0.0f);
    angle = 0.0f;
}


Light::~Light()
{
}


glm::mat4 Light::GetSpaceMatrix()
{
    glm::mat4 lightProjMtx;
    
    if (angle > 0.0f)
    {
        lightProjMtx = glm::perspective(angle, 1.0f, 1.0f, 20.0f);  // (GLfloat)shadowWidth / (GLfloat)shadowHeight
    }
    else
    {
        lightProjMtx = glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 1.0f, 20.0f);
    }
    
    glm::mat4 lightViewMtx = glm::lookAt(position, direction, glm::vec3(0, 1, 0));
    return lightProjMtx * lightViewMtx;
}
