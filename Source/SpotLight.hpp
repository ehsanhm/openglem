//
//  SpotLight.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#ifndef SpotLight_hpp
#define SpotLight_hpp

#include <math.h>
#include <glad.h>
#include <glm.hpp>
#include "PointLight.hpp"

class SpotLight: public PointLight
{
public:
    SpotLight();
    SpotLight(glm::vec3 _color,
              GLfloat _intensity,
              glm::vec3 _position,
              glm::vec3 _direction);
    ~SpotLight();
    void AddLight(Shader* shader);
    void SetDirection(Shader* shader, glm::vec3 _direction);
    GLfloat exponent;
    GLfloat linear;
    GLfloat constant;
    GLfloat softness;
};


#endif /* SpotLight_hpp */
