//
//  Model.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-24.
//

#ifndef Model_hpp
#define Model_hpp

#include <iostream>
#include <string>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Mesh.hpp"
#include "Shader.hpp"


using namespace std;


class Model
{
public:
    Model(Shader* _shader): shader(_shader){};
    Model(Shader* _shader, string path): shader(_shader)
    {
        loadModel(path);
    }
    void Draw(Shader* shader);
    vector<Mesh> meshes;
//    vector<Texture> textures;
    void SetScale(GLfloat scale);
    
    Shader *shader;

private:
    void loadModel(string path);
    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
    vector<Texture> LoadMaterial(aiMesh *mesh, const aiScene *scene);
    string removeSlashes(const string texturePath);
};

#endif /* Model_hpp */
