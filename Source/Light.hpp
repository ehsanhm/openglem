//
//  Light.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-16.
//

#ifndef Light_hpp
#define Light_hpp

#include <iostream>
#include <glad.h>
#include <glm.hpp>
#include "Shader.hpp"

class Light
{
public:
    Light();
    Light(glm::vec3 _color, GLfloat _intensity);
    ~Light();
    
    glm::mat4 GetSpaceMatrix();
    
    glm::vec3 color;
    GLfloat intensity;
    
    glm::vec3 position;
    glm::vec3 direction;
    GLfloat angle;
    
    GLfloat shadowWidth;
    GLfloat shadowHeight;
    
    GLuint positionId;
    GLuint directionId;
    
    GLuint uid;
};

#endif /* Light_hpp */
