//
//  Shapes.hpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-05.
//

#ifndef Shapes_hpp
#define Shapes_hpp

#include <iostream>
#include <vector>
#include <math.h>
#include <glad.h>
#include <Mesh.hpp>

using namespace std;


class Circle2D: public Mesh
{
public:
    Circle2D(GLfloat radius, GLuint num_points);
};


class Pyramid: public Mesh
{
public:
    Pyramid();
};


class Plane: public Mesh
{
public:
    Plane(GLfloat scale);
};

#endif /* Shapes_hpp */
