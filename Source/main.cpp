#define GL_SILENCE_DEPRECATION
#define STB_IMAGE_IMPLEMENTATION

#include <iostream>
#include <vector>
#include <glfw3.h>
#include <glad.h>
#include <glm.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include <algorithm>

//#include "Test.hpp"
#include "Shapes.hpp"
#include "Shader.hpp"
#include "GLWindow.hpp"
#include "Transform.hpp"
#include "Camera.hpp"
#include "Texture.hpp"
#include "Material.hpp"
#include "Light.hpp"
#include "DirectionalLight.hpp"
#include "PointLight.hpp"
#include "SpotLight.hpp"
#include "Model.hpp"
#include "Shadow.hpp"


using namespace std;


// Define main function
int main()
{    
    GLWindow my_window;
    my_window.Create(600, 400);
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // GL Version
    cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
    
    // Create shader
    Shader shd_prog;
    shd_prog.CreateFromFile("Resources/Shaders/BasicShader.glsl");
    shd_prog.SetInt("textureDiffuseA", 0);
    shd_prog.SetInt("textureNormalA", 1);
    shd_prog.SetInt("shadowMapA", 2);
    
//    Shader shd_wireframe;
//    shd_wireframe.CreateFromFile("Resources/Shaders/Wireframe.glsl");
    
    Shader shd_depth;
    shd_depth.CreateFromFile("Resources/Shaders/DepthShader.glsl");
    shd_depth.SetInt("textureDiffuseA", 0);
    
    // Create geo
    Model customModels(&shd_prog);
//    Mesh circle = Circle2D(2.0f, 20);
    Mesh plane = Plane(10.0f);
//    plane.SetTranslate(glm::vec3(0, -2.5, 0));
    Texture concreteTex(&shd_prog, "Resources/Textures/concrete.png", "texture_diffuse");
    concreteTex.LoadTexture();
    plane.textures = {concreteTex};
    customModels.meshes.push_back(plane);

    
//    Model mod("Resources/Meshes/cube_2.obj");
    Model mod(&shd_prog, "Resources/Meshes/plant_01.obj");
//    Model mod(&shd_prog, "Resources/Meshes/airboat.obj");
//    Model mod(&shd_prog, "Resources/Meshes/sphere.obj");
    mod.SetScale(0.1f);
    
    
    // Temporarily bind vertex array so we can compile shaders
    glBindVertexArray(mod.meshes[0].VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mod.meshes[0].VBO);
    
////    // Load Texture
//    Texture uvTex("Resources/Textures/uv_checker.png");
//    Texture concreteTex("Resources/Textures/concrete.png"); //, "diffuse_color"
//    Texture white("Resources/Textures/white.png");
    
    // Material
    Material mat(shd_prog);
    mat.diffuse = 1.0f;
    mat.specular = 1.0f;
    mat.emissionColor = glm::vec3(0.4f, 0.7f, 1.0f);
    mat.emission = 0.5f;
    
    // Camera
    Camera cam;
    
    // Lights
//    DirectionalLight sun;
//    sun.intensity = 1.0f;
//    sun.color = glm::vec3(1.0f, 0.8f, 0.6f);
//    sun.position = glm::vec3(-2.0f, 4.0f, 2.0f);
//    sun.direction = glm::vec3(2.0f, -4.0f, -2.0f);
//    sun.AddLight(&shd_prog);
    
//    PointLight lamp1;
//    lamp1.intensity = 3.0f;
//    lamp1.exponent = 0.5f;
//    lamp1.color = glm::vec3(1.0f, 0.8f, 0.6f);
//    lamp1.position = glm::vec3(-2.0f, 1.0f, 2.0f);
//    lamp1.AddLight(&shd_prog);
//
//    PointLight lamp2;
//    lamp2.intensity = 3.0f;
//    lamp2.exponent = 0.5f;
//    lamp2.color = glm::vec3(0.6f, 0.8f, 1.0f);
//    lamp2.position = glm::vec3(2.0f, 1.0f, 2.0f);
//    lamp2.AddLight(&shd_prog);
//
//    PointLight lamp3;
//    lamp3.intensity = 3.0f;
//    lamp3.exponent = 0.5f;
//    lamp3.color = glm::vec3(0.6f, 0.6f, 1.0f);
//    lamp3.position = glm::vec3(0.0f, 1.0f, -2.0f);
//    lamp3.AddLight(&shd_prog);
    
    // Spot lights
    SpotLight flashLight1;
    flashLight1.intensity = 60.0f;
    flashLight1.linear = 0.0f;
    flashLight1.exponent = 1.0f;
    flashLight1.color = glm::vec3(0.9f, 0.9f, 0.9f);
    flashLight1.position = glm::vec3(-3.0f, 4.0f, 3.0f);
    flashLight1.direction = glm::vec3(0.0f, 1.0f, -0.0f);
    flashLight1.angle = 20.0f;
    flashLight1.softness = 0.5f;
    flashLight1.AddLight(&shd_prog);
    
    // Shadow
    Shadow shadow(my_window.width, my_window.height, &flashLight1, &shd_depth);
    
    // Projection
    glm::mat4 projectionMatrix = glm::perspective(45.0f, (GLfloat)my_window.width / (GLfloat)my_window.height, 0.1f, 50.0f);
    
    // delta time
    GLfloat deltaTime=0.0f, lastTime=0.0f, now = 0.0f;
    
    
    glm::mat4 lightSpaceMtx = flashLight1.GetSpaceMatrix();
    shd_depth.SetMat4("lightSpaceMatrix", lightSpaceMtx);
    shd_prog.SetMat4("lightSpaceMatrix", lightSpaceMtx);
    shd_depth.SetMat4("modelMatrix", glm::mat4(1.0f));
    shd_prog.SetMat4("modelMatrix", glm::mat4(1.0f));
    
    
    // Event loop
    while(!my_window.ShouldClose())
    {
        // get and handle user inputs events
        glfwPollEvents();

        // Clear window
        glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
//        glDepthFunc(GL_ALWAYS);

        
        // deltatime
        now = glfwGetTime();
        deltaTime = now - lastTime;
        lastTime = now;
        
        // Camera inputs
        cam.HandleKey(my_window.getKeys(), deltaTime);
        cam.HandleMouse(my_window.GetXChange(), my_window.GetYChange());
        
//        // Move light
//        flashLight1.SetPosition(&shd_prog, cam.position + glm::vec3(0, -0.5f, 0));
//        flashLight1.SetDirection(&shd_prog, cam.position + glm::vec3(0, -0.5f, 0) + cam.front);
        
        // trasform matrix
        Transform trs_mtx;
//        trs_mtx.Translate(0.0f, 0.0f, -2.0f);
//        trs_mtx.Scale(0.3f, 0.3f, 0.3f);
        
        // Create shadow map
        shadow.PrepareToRender();
        customModels.Draw(&shd_depth);
        mod.Draw(&shd_depth);
        shadow.FinishRender();
        
        // Shader
//        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mat.UseMaterial();
        shd_prog.SetMat4("viewMatrix", cam.GetMatrix());
        shd_prog.SetMat4("projectionMatrix", projectionMatrix);
        shd_prog.SetVec3("cameraPosition", cam.GetPosition());
        
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, shadow.shadowMap);
        
        // Render
        customModels.Draw(&shd_prog);
        mod.Draw(&shd_prog);
        
//        // Ground
//        ground.Draw();
        
//        // Other geos
//        for (auto geo: meshes)
//        {
//            geo->RenderMesh();
//        }

//        // Wireframe
//        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//        shd_wireframe.SetModelMatrix(trs_mtx.matrix);
//        shd_wireframe.SetViewMatrix(cam.GetMatrix());
//        shd_wireframe.SetProjectionMatrix(projectionMatrix);
//        shd_wireframe.UseShader();
//
//        customModels.Draw();
//        mod.Draw();
        
//        for (Mesh* geo: meshes)
//        {
//            geo->RenderMesh();
//        }

        // swap buffers
        my_window.SwapBuffers();
    }
    
//    // Free memory
//    for (auto geo: meshes)
//    {
//        delete geo;
//    }
//    meshes.clear();

    // Terminate GLFW
    glfwTerminate();
    return 0;
}
