//
//  Mesh.hpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-05.
//

#ifndef Mesh_hpp
#define Mesh_hpp

#include <iostream>
#include <string>
#include <vector>
#include <glad.h>
#include <glm.hpp>
#include "Texture.hpp"


using namespace std;


class Vertex
{
public:
    glm::vec3 position;
    glm::vec2 uv;
    glm::vec3 normal;
    Vertex(){};
    Vertex(glm::vec3 _pos, glm::vec2 _uv, glm::vec3 _norm):
        position(_pos), uv(_uv), normal(_norm){}
};


class Mesh
{
    
public:
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    vector<Texture> textures;
    
    Mesh();
    Mesh(vector<Vertex> _vertices, vector<unsigned int> _indices, vector<Texture> _textures);
    Mesh SetTranslate(glm::vec3 translate);
    Mesh SetScale(GLfloat scale);
    void LoadTextures();
    void UseTextures(Shader* shader);
    void Draw();
    
//private:
    unsigned int VAO, VBO, EBO;
    
    void SetupMesh();
};

#endif /* Mesh_hpp */
