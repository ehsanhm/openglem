//
//  DirectionalLight.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-17.
//

#ifndef DirectionalLight_hpp
#define DirectionalLight_hpp

#include <glad.h>
#include <glm.hpp>
#include "Light.hpp"

class DirectionalLight: public Light
{
public:
    
    void AddLight(Shader* shader);        
};

#endif /* DirectionalLight_hpp */
