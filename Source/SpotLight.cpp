//
//  SpotLight.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#include "SpotLight.hpp"


SpotLight::SpotLight()
{
    position = glm::vec3(0.0f, 5.0f, 0.0f);
    direction = glm::vec3(0.0f, 0.0f, 0.0f);
    constant = 1.0f;
    linear = 0.0f;
    exponent = 0.5f;
    angle = 20.0f;
    softness = 0.0f;
}


SpotLight::SpotLight(glm::vec3 _color,
                     GLfloat _intensity,
                     glm::vec3 _position,
                     glm::vec3 _direction)
{
    color = _color;
    intensity = _intensity;
    position = _position;
    direction = _direction;
    constant = 1.0f;
    linear = 0.0f;
    exponent = 0.5f;
    angle = 20.0f;
    softness = 0.0f;
}


SpotLight::~SpotLight()
{
}

void SpotLight::AddLight(Shader* shader)
{
    shader->UseShader();
    
    positionId = shader->spotLightsPositionsIds[shader->spotLightsInUse];
    directionId = shader->spotLightsDirectionsIds[shader->spotLightsInUse];
    
    glUniform3f(positionId, position.x, position.y, position.z);
    glUniform3f(directionId, direction.x, direction.y, direction.z);
    glUniform3f(shader->spotLightsColorsIds[shader->spotLightsInUse], color.x, color.y, color.z);
    glUniform1f(shader->spotLightsIntensitiesIds[shader->spotLightsInUse], intensity);
    glUniform1f(shader->spotLightsExponentsIds[shader->spotLightsInUse], exponent);
    glUniform1f(shader->spotLightsLinearsIds[shader->spotLightsInUse], linear);
    glUniform1f(shader->spotLightsConstantsIds[shader->spotLightsInUse], constant);
    glUniform1f(shader->spotLightsAnglesIds[shader->spotLightsInUse], std::cosf(glm::radians(angle)));
    glUniform1f(shader->spotLightsSoftnessesIds[shader->spotLightsInUse], softness);
    
    shader->spotLightsInUse ++;
    glUniform1i(shader->spotLightsInUseId, shader->spotLightsInUse);
}


void SpotLight::SetDirection(Shader* shader, glm::vec3 _direction)
{
    shader->UseShader();
    direction = _direction;
    glUniform3f(directionId, direction.x, direction.y, direction.z);
}
