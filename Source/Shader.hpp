//
//  Shader.hpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-07.
//

#ifndef Shader_hpp
#define Shader_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <glad.h>
#include <glm.hpp>
#include <GLM/gtc/type_ptr.hpp>


using namespace std;


const unsigned int MAX_NUM_OF_DIRECTIONAL_LIGHTS = 4;
const unsigned int MAX_NUM_OF_POINT_LIGHTS = 4;
const unsigned int MAX_NUM_OF_SPOT_LIGHTS = 4;

//static int NUM_LIGHTS = 0;

struct ShaderProgramSource
{
    string vertexShader;
    string fragmentShader;
};


class Shader
{
public:
    Shader();
    ~Shader();
    
    void CreateFromString(string vertexCode, string fragmentCode);
    void CreateFromFile(string fileName);
    void UseShader();
    void DeleteShader();
    
    void SetModelMatrix(glm::mat4 modelMatrix);
    void SetViewMatrix(glm::mat4 viewMatrix);
    void SetProjectionMatrix(glm::mat4 projectionMatrix);
    
    void SetCameraPosition(glm::vec3 cameraPosition);
        
    void SetMaterialDiffuseColor(glm::vec3 color);
    void SetMaterialDiffuse(GLfloat value);
    void SetMaterialSpecularColor(glm::vec3 color);
    void SetMaterialSpecular(GLfloat value);
    void SetMaterialEmissionColor(glm::vec3 color);
    void SetMaterialEmission(GLfloat value);
    
    void SetTextureDiffuseAId(GLuint value);
    void SetTextureNormalAId(GLuint value);
    void SetInt(const char *item, GLuint value);
    void SetMat4(const char *item, glm::mat4 value);
    void SetVec3(const char *item, glm::vec3 value);
    
//private:
    void AddShader(GLuint program, string shd, GLenum shaderType);
    GLuint id;
    
    string vertexShaderString;
    string fragmentShaderString;
    
    
    GLuint modelMatrixId;
    GLuint viewMatrixId;
    GLuint projectionMatrixId;
    
    GLuint diffuseColorId;
    GLuint diffuseId;
    GLuint specularColorId;
    GLuint specularId;
    GLuint emissionColorId;
    GLuint emissionId;
    
    GLuint textureDiffuseAId;
    GLuint textureNormalAId;
    
    GLuint cameraPositionId;
    
    GLuint directionalLightsInUse;
    GLuint directionalLightsInUseId;
    GLuint directionalLightsColorsIds[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    GLuint directionalLightsPositionsIds[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    GLuint directionalLightsDirectionsIds[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    GLuint directionalLightsIntensitiesIds[MAX_NUM_OF_DIRECTIONAL_LIGHTS];
    GLuint directionalLightsSpaceMatricesIds[MAX_NUM_OF_DIRECTIONAL_LIGHTS];    
    
    GLuint pointLightsInUse;
    GLuint pointLightsInUseId;
    GLuint pointLightsColorsIds[MAX_NUM_OF_POINT_LIGHTS];
    GLuint pointLightsPositionsIds[MAX_NUM_OF_POINT_LIGHTS];
    GLuint pointLightsIntensitiesIds[MAX_NUM_OF_POINT_LIGHTS];
    GLuint pointLightsExponentsIds[MAX_NUM_OF_POINT_LIGHTS];
    GLuint pointLightsLinearsIds[MAX_NUM_OF_POINT_LIGHTS];
    GLuint pointLightsConstantsIds[MAX_NUM_OF_POINT_LIGHTS];
    
    GLuint spotLightsInUse;
    GLuint spotLightsInUseId;
    GLuint spotLightsColorsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsPositionsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsDirectionsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsIntensitiesIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsExponentsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsLinearsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsConstantsIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsAnglesIds[MAX_NUM_OF_SPOT_LIGHTS];
    GLuint spotLightsSoftnessesIds[MAX_NUM_OF_SPOT_LIGHTS];
};



#endif /* Shader_hpp */
