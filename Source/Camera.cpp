//
//  Camera.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-10.
//

#include "Camera.hpp"


Camera::Camera()
{
    position = glm::vec3(0.0f, 1.0f, 3.0f);
    aim = glm::vec3(0.0f, 0.0f, 0.0f);
    up = glm::vec3(0.0f, 1.0f, 0.0f);
    worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
    right = glm::vec3(1.0f, 0.0f, 0.0f);
    front = glm::vec3(0.0f, 0.0f, -1.0f);
    moveSpeed = 5.0f;
    turnSpeed = 1.0f;
    yaw = -90.0f;
    pitch = 0.0f;
    useAim = false;
    Fly();
}


Camera::~Camera()
{
}


void Camera::Fly()
{    
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(front);
    
    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}


void Camera::HandleKey(const bool *keys, float deltaTime)
{
    float velocity = moveSpeed * deltaTime;
    
    if (keys[GLFW_KEY_W])
    {
        position += front * velocity;
    }
    
    if (keys[GLFW_KEY_S])
    {
        position -= front * velocity;
    }
    
    if (keys[GLFW_KEY_A])
    {
        position -= right * velocity;
    }
    
    if (keys[GLFW_KEY_D])
    {
        position += right * velocity;
    }
    
    if (keys[GLFW_KEY_Q])
    {
        position -= up * velocity;
    }
    
    if (keys[GLFW_KEY_E])
    {
        position += up * velocity;
    }
}


void Camera::HandleMouse(GLfloat xChange, GLfloat yChange)
{
    xChange *= turnSpeed;
    yChange *= turnSpeed;
    yaw += xChange;
    pitch += yChange;
    if (pitch > 89.0f) pitch = 89.0f;
    if (pitch < -89.0f) pitch = -89.0f;
    Fly();
}


glm::mat4 Camera::GetMatrix()
{
    glm::mat4 mat = glm::lookAt(position, position + front, up);
    return mat;
}


glm::vec3 Camera::GetPosition()
{
    return position;
}
