//
//  PointLight.cpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-21.
//

#include "PointLight.hpp"


PointLight::PointLight()
{
    position = glm::vec3(0.0f, 5.0f, 0.0f);
    constant = 1.0f;
    linear = 0.0f;
    exponent = 0.0f;
}


PointLight::PointLight(glm::vec3 _color,
                       GLfloat _intensity,
                       glm::vec3 _position)
{
    color = _color;
    intensity = _intensity;
    position = _position;
    constant = 1.0f;
    linear = 0.0f;
    exponent = 0.0f;
}


PointLight::~PointLight()
{
}

void PointLight::AddLight(Shader* shader)
{
    shader->UseShader();
    
    positionId = shader->pointLightsPositionsIds[shader->pointLightsInUse];
    
    glUniform3f(shader->pointLightsColorsIds[shader->pointLightsInUse],
                color.x, color.y, color.z);

    glUniform3f(positionId,
                position.x, position.y, position.z);

    glUniform1f(shader->pointLightsIntensitiesIds[shader->pointLightsInUse],
                intensity);
    
    glUniform1f(shader->pointLightsExponentsIds[shader->pointLightsInUse],
                exponent);
    
    glUniform1f(shader->pointLightsLinearsIds[shader->pointLightsInUse],
                linear);
    
    glUniform1f(shader->pointLightsConstantsIds[shader->pointLightsInUse],
                constant);
    
    shader->pointLightsInUse ++;
    glUniform1i(shader->pointLightsInUseId, shader->pointLightsInUse);
}


void PointLight::SetPosition(Shader* shader, glm::vec3 _position)
{
    shader->UseShader();
    position = _position;
    glUniform3f(positionId, _position.x, _position.y, _position.z);
}
