//
//  Shader.cpp
//  Udemy_OpenGL_003
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-07.
//

#include "Shader.hpp"

Shader::Shader()
{
    directionalLightsInUse = 0;
    pointLightsInUse = 0;
    spotLightsInUse = 0;
}


Shader::~Shader()
{
    DeleteShader();
}


void Shader::AddShader(GLuint program, string shd, GLenum shaderType)
{
    GLuint shader = glCreateShader(shaderType);
    const char* shd_str = shd.c_str();
    glShaderSource(shader, 1, &shd_str, nullptr);
    glCompileShader(shader);

    GLint result;
    GLchar elog[1024];

    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (!result)
    {
        glGetShaderInfoLog(shader, sizeof(elog), NULL, elog);
        string shader_type = (shaderType == GL_VERTEX_SHADER) ? "Vertex" : "Fragment";
        cout << "Error compiling " << shader_type << " shader: " << elog << endl;
    }

    glAttachShader(program, shader);

}


void Shader::CreateFromString(string v_shd, string f_shd)
{

    id = glCreateProgram();

    AddShader(id, v_shd, GL_VERTEX_SHADER);
    AddShader(id, f_shd, GL_FRAGMENT_SHADER);

    GLint result;
    GLchar elog[1024];

    glLinkProgram(id);
    glGetProgramiv(id, GL_LINK_STATUS, &result);
    if (!result)
    {
        glGetProgramInfoLog(id, sizeof(elog), NULL, elog);
        cout << "Error linking shader program: " << elog << endl;
    }

    glValidateProgram(id);
    glGetProgramiv(id, GL_VALIDATE_STATUS, &result);
    if (!result)
    {
        glGetProgramInfoLog(id, sizeof(elog), NULL, elog);
        cout << "Error validating shader program: " << elog << endl;
    }
    
    modelMatrixId = glGetUniformLocation(id, "modelMatrix");
    viewMatrixId = glGetUniformLocation(id, "viewMatrix");
    projectionMatrixId = glGetUniformLocation(id, "projectionMatrix");

    cameraPositionId = glGetUniformLocation(id, "cameraPosition");
    
    // get uniform locations for material
    diffuseColorId = glGetUniformLocation(id, "material.diffuseColor");
    diffuseId = glGetUniformLocation(id, "material.diffuse");
    specularColorId = glGetUniformLocation(id, "material.specularColor");
    specularId = glGetUniformLocation(id, "material.specular");
    emissionColorId = glGetUniformLocation(id, "material.emissionColor");
    emissionId = glGetUniformLocation(id, "material.emission");
    
    //
    textureDiffuseAId = glGetUniformLocation(id, "textureDiffuseA");
    textureNormalAId = glGetUniformLocation(id, "textureNormalA");
    
    // get uniform locations of directional lights from shader
    directionalLightsInUseId = glGetUniformLocation(id, "directionalLightsInUse");
    for(int i = 0; i < 4; i++)
    {
        stringstream ss;
        string s;
        ss << "directionalLights[" << i << "].color";
        ss >> s;
        directionalLightsColorsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "directionalLights[" << i << "].direction";
        ss >> s;
        directionalLightsDirectionsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "directionalLights[" << i << "].position";
        ss >> s;
        directionalLightsPositionsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "directionalLights[" << i << "].intensity";
        ss >> s;
        directionalLightsIntensitiesIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "directionalLights[" << i << "].spaceMatrix";
        ss >> s;
        directionalLightsSpaceMatricesIds[i] = glGetUniformLocation(id, s.c_str());
    }

    // get uniform locations of point lights from shader
    pointLightsInUseId = glGetUniformLocation(id, "pointLightsInUse");
    for(int i = 0; i < 4; i++)
    {
        stringstream ss;
        string s;
        ss << "pointLights[" << i << "].color";
        ss >> s;
        pointLightsColorsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "pointLights[" << i << "].position";
        ss >> s;
        pointLightsPositionsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "pointLights[" << i << "].intensity";
        ss >> s;
        pointLightsIntensitiesIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "pointLights[" << i << "].exponent";
        ss >> s;
        pointLightsExponentsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "pointLights[" << i << "].linear";
        ss >> s;
        pointLightsLinearsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "pointLights[" << i << "].constant";
        ss >> s;
        pointLightsConstantsIds[i] = glGetUniformLocation(id, s.c_str());
    }
    
    // get uniform locations of spot lights from shader
    spotLightsInUseId = glGetUniformLocation(id, "spotLightsInUse");
    for(int i = 0; i < 4; i++)
    {
        stringstream ss;
        string s;
        ss << "spotLights[" << i << "].base.color";
        ss >> s;
        spotLightsColorsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].base.position";
        ss >> s;
        spotLightsPositionsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].direction";
        ss >> s;
        spotLightsDirectionsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].base.intensity";
        ss >> s;
        spotLightsIntensitiesIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].base.exponent";
        ss >> s;
        spotLightsExponentsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].base.linear";
        ss >> s;
        spotLightsLinearsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].base.constant";
        ss >> s;
        spotLightsConstantsIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].angle";
        ss >> s;
        spotLightsAnglesIds[i] = glGetUniformLocation(id, s.c_str());
        
        stringstream().swap(ss);
        ss << "spotLights[" << i << "].softness";
        ss >> s;
        spotLightsSoftnessesIds[i] = glGetUniformLocation(id, s.c_str());
    }
    
}


void Shader::CreateFromFile(string fileName)
{
    vertexShaderString = "";
    fragmentShaderString = "";
    
    bool belongsToVertexShader = true;
    
    
    string line;
    ifstream my_stream(fileName);
    
    if (my_stream.is_open())
    {
        while (getline(my_stream, line))
        {
            if (line.find("# vertexShader") != string::npos)
            {
                belongsToVertexShader = true;
            }
            
            else if (line.find("# fragmentShader") != string::npos)
            {
                belongsToVertexShader = false;
            }
            
            else
            {
                if (belongsToVertexShader)
                {
                    vertexShaderString += line + "\n";
                }
                else
                {
                    fragmentShaderString += line + "\n";
                }
            }
        }
    }
    else
    {
        cout << "Unable to open file: " << fileName << endl;
    }
    
    CreateFromString(vertexShaderString, fragmentShaderString);
}


void Shader::UseShader()
{
    glUseProgram(id);
    
}


void Shader::DeleteShader()
{
    if (id != 0)
    {
        glDeleteProgram(id);
        id = 0;
    }
    
    modelMatrixId = 0;
    viewMatrixId = 0;
    projectionMatrixId = 0;
    cameraPositionId = 0;
    
    diffuseColorId = 0;
    diffuseId = 0;
    specularColorId = 0;
    specularId = 0;
    emissionColorId = 0;
    emissionId = 0;
    
    directionalLightsInUseId = 0;
    pointLightsInUseId = 0;
    spotLightsInUseId = 0;
}


void Shader::SetModelMatrix(glm::mat4 modelMatrix)
{
    UseShader();
    glUniformMatrix4fv(modelMatrixId, 1, GL_FALSE, glm::value_ptr(modelMatrix));
}


void Shader::SetViewMatrix(glm::mat4 viewMatrix)
{
    UseShader();
    glUniformMatrix4fv(viewMatrixId, 1, GL_FALSE, glm::value_ptr(viewMatrix));
}


void Shader::SetProjectionMatrix(glm::mat4 projectionMatrix)
{
    UseShader();
    glUniformMatrix4fv(projectionMatrixId, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
}

void Shader::SetMaterialSpecularColor(glm::vec3 color)
{
    UseShader();
    glUniform3f(specularColorId, color.x, color.y, color.z);
}

void Shader::SetMaterialSpecular(GLfloat value)
{
    UseShader();
    glUniform1f(specularId, value);
}

void Shader::SetMaterialDiffuseColor(glm::vec3 color)
{
    UseShader();
    glUniform3f(diffuseColorId, color.x, color.y, color.z);
}

void Shader::SetMaterialDiffuse(GLfloat value)
{
    UseShader();
    glUniform1f(diffuseId, value);
}

void Shader::SetMaterialEmissionColor(glm::vec3 color)
{
    UseShader();
    glUniform3f(emissionColorId, color.x, color.y, color.z);
}

void Shader::SetMaterialEmission(GLfloat value)
{
    UseShader();
    glUniform1f(emissionId, value);
}

void Shader::SetCameraPosition(glm::vec3 cameraPosition)
{
    UseShader();
    glUniform3f(cameraPositionId, cameraPosition.x, cameraPosition.y, cameraPosition.z);
}


void Shader::SetTextureDiffuseAId(GLuint value)
{
    UseShader();
    glUniform1i(textureDiffuseAId, value);
}


void Shader::SetTextureNormalAId(GLuint value)
{
    UseShader();
    glUniform1i(textureNormalAId, value);
}


void Shader::SetInt(const char *item, GLuint value)
{
    UseShader();
    glUniform1i(glGetUniformLocation(id, item), value);
}


void Shader::SetMat4(const char *item, glm::mat4 value)
{
    UseShader();
    glUniformMatrix4fv(glGetUniformLocation(id, item), 1, GL_FALSE, glm::value_ptr(value));
}


void Shader::SetVec3(const char *item, glm::vec3 value)
{
    UseShader();
    glUniform3f(glGetUniformLocation(id, item), value.x, value.y, value.z);
}
