//
//  Camera.hpp
//  OpenGLEM
//
//  Created by Ehsan Hassani Moghaddam on 2022-05-10.
//

#ifndef Camera_hpp
#define Camera_hpp

#include <iostream>
#include <glm.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include <GLM/gtc/quaternion.hpp>
#include <GLM/gtx/quaternion.hpp>
#include <glfw3.h>

using namespace std;

class Camera
{

public:
    Camera();
    ~Camera();
    void HandleKey(const bool *keys, float deltaTime);
    void HandleMouse(GLfloat xChange, GLfloat yChange);
    void Fly();
    glm::mat4 GetMatrix();
    glm::vec3 GetPosition();
    glm::mat4 matrix;
    glm::vec3 position;
    glm::vec3 aim;
    glm::vec3 up;
    glm::vec3 worldUp;
    glm::vec3 right;
    glm::vec3 front;
    float yaw;
    float pitch;
    float moveSpeed;
    float turnSpeed;
    bool useAim;

//private:
};

#endif /* Camera_hpp */
